# Rocket League video goal classifier

This project is based on https://github.com/harvitronix/five-video-classification-methods , go check it out if you want to learn some more about neural networks for video classification!

In my free time, I like to play Rocket League and sometimes record videos of my gameplay - you never know when you will pull off that sick move that you just NEED to show to someone, right?
At some point I wanted to make a compilation of my best goals, but found it way too tedious to watch the hours of gameplay all by myself.
That's why I trained a neural net to do just that: Analyze a video, recognize the scenes where a goal is being shot, and save these scenes as new videos.

Now the challenge with analyzing Rocket League videos is
1) the very different appearances of 'goals' - what goal explosion is used, what arena/background is there, is there GUI that overlaps the goal scene etc.
2) the high detail level one needs to really recognize a goal. For lots of other video classification tasks, very low resolution videos suffice for good classification. In our case, we want the resoultion to be as high as possible (where the data should still fit into VRAM).

As this is my first self-built neural network, the quality of the classification is still not perfect. In particular, it can happen that a scene is being classified as 'goal' even without a goal happening.
That being said, don't expect a too high accuracy.


## Requirements

This code requires you have Keras 2 and TensorFlow 1 or greater installed. Please see the `requirements.txt` file. To ensure you're up to date, run:

`pip install -r requirements.txt`

You must also have ffmpeg and ffprobe available, which is why I included the .exe files of them in this project. You can leave them where they are, they will be used from there.


## What does it do?

1) Extract every 2nd (or so, dependend on framerate) frame from the given video using ffmpeg (only use every second frame to finish the prediction in half the time)
2) combine each 40 extracted frames into a sequences, for which a prediction will be made. So effectively, a video with 240 frames will be extracted to 120 frame images, which will be built into 3 sequences of 40 frames each.
3) save the intervals, in which p(goal) > 0.5, in a list.
4) after we made a prediction for all sequences, extract the intervals with goals from the original video, and save them as numerated video files in the RocketLeague_GoalClassifier folder.


## What model did you use

I used a convolutional LSTM (ConvLSTM2D in Keras), with a convolution step before the convLSTM and a small fully connected layer afterwards. Concretely:
```

Layer (type)                 Output Shape              Param #
=================================================================
conv1 (Conv3D)               (None, 40, 135, 240, 32)  2624
_________________________________________________________________
max_pooling3d_1 (MaxPooling3 (None, 40, 45, 80, 32)    0
_________________________________________________________________
dropout_1 (Dropout)          (None, 40, 45, 80, 32)    0
_________________________________________________________________
conv_lst_m2d_1 (ConvLSTM2D)  (None, 40, 23, 40, 40)    103840
_________________________________________________________________
batch_normalization_1 (Batch (None, 40, 23, 40, 40)    160
_________________________________________________________________
conv_lst_m2d_2 (ConvLSTM2D)  (None, 40, 12, 20, 40)    115360
_________________________________________________________________
batch_normalization_2 (Batch (None, 40, 12, 20, 40)    160
_________________________________________________________________
flatten_1 (Flatten)          (None, 384000)            0
_________________________________________________________________
dropout_2 (Dropout)          (None, 384000)            0
_________________________________________________________________
dense_1 (Dense)              (None, 32)                12288032
_________________________________________________________________
dropout_3 (Dropout)          (None, 32)                0
_________________________________________________________________
dense_2 (Dense)              (None, 2)                 66
=================================================================
Total params: 12,510,242
Trainable params: 12,510,082
Non-trainable params: 160
_________________________________________________________________
```



## My dataset

Unfortunately, my dataset of Rocket League clips I used to train the net, contains some parts I'd like to keep private. If you want to train the net youself, you can collect 3-10 second clips of both goals and non-goals, and use them to train.
My dataset currently contains about 400 clips.

If you have a dataset and put the clips into `./data/train/Goal` or `./data/train/NoGoal` respectively , you can run `python 2_extract_files.py` from the data folder in order to extract all frames from the videos.
These will be used to build the sequences we train our model on. For more detail, read https://github.com/harvitronix/five-video-classification-methods !

## Training models

The model can be run from `train.py`. There are configuration options you can set in that file to choose what batch size or image size you want to use, or whether you want to train a different model than my standard one.

The models are all defined in `models.py`. Reference that file to see which models you are able to run in `train.py`.

Training logs are saved to CSV and also to TensorBoard files. To see progress while training, run `tensorboard --logdir=data/logs` from the project root folder.

## Demo/Using models

You can use the trained model on a video file by using `python demo.py "absolute/path/to/video.mp4"`. The optional 'goal_offset' parameter allows you to set the amount of frames before and after a goal scene that should be included in the extracted video files.
E.g. 140 frames resembles 2.33 second on a 60 fps video, or 4.66 seconds on a 30 fps video.

## I want to use the program, but don't want to setup python/tensorflow/...

Lucky you, I compiled the program to an .exe file, and even wrote a (very simplistic) GUI. Download it at https://gitlab.com/chrifuss/rocketleague_goalclassifier_exe and use the GUI.exe (leave it in the download folder though, as it needs ffmpeg and will also output the extracted videos to this folder)
