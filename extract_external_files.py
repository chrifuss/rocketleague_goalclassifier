"""
After moving all the files using the 1_ file, we run this one to extract
the images from the videos and also create a data file we can use
for training and testing later.
"""
import csv
import glob
import os
import shutil
import os.path
import subprocess
import sys
import ffmpeg
import cv2

def extract_file(filepath, sequence_length_modificator):
    """After we have all of our videos split between train and test, and
    all nested within folders representing their classes, we need to
    make a data file that we can reference when training our RNN(s).
    This will let us keep track of image sequences and other parts
    of the training process.

    We'll first need to extract images from each of the videos. We'll
    need to record the following data in the file:

    [train|test], class, filename, nb frames

    Extracting can be done with ffmpeg:
    `ffmpeg -i video.mpg image-%04d.jpg`
    """
    data_file = []
    
    newpath = 'video_processed_tmp' 
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    
    #filepath = glob.glob(os.path.join(filepath, '*.mp4'))

    #for video_path in video_files:
    filename_no_ext = filepath.replace('/','\\').split(os.path.sep)[-1].split('.mp4')[0].split('.avi')[0]
    print(filename_no_ext)
    if not check_already_extracted(filename_no_ext):
        dest = 'video_processed_tmp\\' + filename_no_ext + '-%04d.jpg'
        print(filepath)
        print(dest)
        print("framerate: " + str(get_frame_rate(filepath)))
        # only extract every n-th frame in the ffmpeg call using sequence_length_modificator
        subprocess.call(["ffmpeg.exe", "-fflags", "+genpts", "-i", filepath, "-vf" ,"scale=640:360", "-r", str(get_frame_rate(filepath)/sequence_length_modificator) , "-threads", "16", "-thread_type", "slice", "-slices", "16", "-q:v", "1", dest], close_fds=True)

        # Now get how many frames it is.
        nb_frames = len(glob.glob(filepath.split('.')[0]+ '*.jpg'))

        data_file.append([filename_no_ext, nb_frames])

        print("Generated %d frames for %s" % (nb_frames, filename_no_ext))

    with open('data_file.csv', 'w') as fout:
        writer = csv.writer(fout)
        writer.writerows(data_file)

    print("Extracted and wrote %d video files." % (len(data_file)))
    
    nb_frames = len(glob.glob('video_processed_tmp\\' + filename_no_ext + '*.jpg'))
    return nb_frames

    
def get_frame_rate(filename):
    if not os.path.exists(filename):
        sys.stderr.write("ERROR: filename %r was not found!" % (filename,))
        return -1         
    out = subprocess.check_output(["ffprobe.exe",filename,"-v","0","-select_streams","v","-print_format","flat","-show_entries","stream=r_frame_rate"])
    print(out)
    rate = str(out).split('=')[1].strip()[1:-1].split('/')
    if len(rate)==1:
        return float(rate[0])
    if len(rate)==2:
        return float(rate[0])/float(rate[1].split("\"")[0])
    return -1
    
def check_already_extracted(filename_no_ext):
    """Check to see if we created the -0001 frame of this file."""
    return bool(os.path.exists(os.path.join("video_processed_tmp", filename_no_ext, '-0001.jpg')))

def delete_generated_image_files(folderpath):
    folder = folderpath
    for file in os.listdir(folder):
        file_path = os.path.join(folder, file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)
            
            
def generate_video_from_framelist(filename, output_name, framelist, framerate, sequence_length_modificator):
    subprocess.call(["ffmpeg.exe", "-y",  "-ss", str(float(framelist[0]*sequence_length_modificator)/float(framerate)), "-i", filename, "-t", str((float(framelist[-1]*sequence_length_modificator) - float(framelist[0]*sequence_length_modificator))/float(framerate)), "-c", "copy", "-async", "1", "-threads", "16", "-thread_type", "slice", "-slices", "16", str(output_name+".mp4")])
    
def main(args):
    """
    Extract images from videos and build a new file that we
    can use as our data input file. It can have format:

    [train|test], class, filename, nb frames
    """
    extract_file(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
