import os
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
from tensorflow.keras.models import load_model
from tensorflow.keras import backend
from data import DataSet
import numpy as np
import extract_files
import argparse
import sys
import os
import subprocess
import extract_external_files
from decimal import Decimal
from pathlib import Path
import gc
import glob
import multiprocessing
import tensorflow as tf
#tf.logging.set_verbosity(tf.logging.INFO)
#tf.compat.v1.disable_eager_execution()

physical_devices = tf.config.list_physical_devices('GPU')
if len(physical_devices) >= 1:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)


def predict(data_type, seq_length, saved_model, image_shape, video_name, class_limit, firstFrame, lastFrame):
    model = load_model(saved_model)

    if image_shape is None:
        data = DataSet(seq_length=seq_length, class_limit=class_limit)
    else:
        data = DataSet(seq_length=seq_length, image_shape=image_shape,
            class_limit=class_limit)
    
    sample = data.get_frames_by_external_filename(video_name, firstFrame, lastFrame, data_type)
    prediction = model.predict(np.expand_dims(sample, axis=0))
    data.print_class_from_prediction(np.squeeze(prediction, axis=0))
    if(prediction[0][0] >= prediction[0][1]):
        return True
    else:
        return False
        

def main(*args, **kwargs):
	
    # model can be one of lstm, lrcn, mlp, conv_3d, c3d.
    model = 'conv_3d'
    # Must be a weights file.
    saved_model = 'data/checkpoints/convlstm-images.020-0.191.h5'
    # Sequence length must match the lengh used during training.
    seq_length = 40
    # Limit must match that used during training.
    class_limit = None
    
    # if a goal offset is specified by 'goal_offset' cmd line parameter, we use it (made for the GUI). Otherwise, choose 4*seq_length
    goal_offset = int(kwargs.get('goal_offset', 3*seq_length))
    saved_model = kwargs.get('saved_model', saved_model)
    print(args)
    
    if len(args) > 1:
        video_name = args[1]
    elif len(args) > 0:
        video_name = args[0]
        
    video_part = 1
    number_of_processes = 1
    
    if len(args) > 2:
        video_part = args[2]
    if len(args) > 3:
        number_of_processes = args[2]
    framerate = extract_external_files.get_frame_rate(video_name)
        
    # only extract every n-th frame from the video with ffmpeg
    sequence_length_modificator = framerate/20.0
    
    print("video_part, number_of_processes: " + str(video_part) + " , " +  str(number_of_processes))
    print("video_name: " + video_name)
    print("goal_offset: " + str(goal_offset))
    print("used model checkpoint: " + saved_model)
	
    lastVideoFrame = extract_external_files.extract_file(video_name, sequence_length_modificator)

    # Chose images or features and image shape based on network.
    if model in ['conv_3d', 'c3d', 'lrcn', 'convlstm']:
        data_type = 'images'
        image_shape = (135, 240, 3)
    elif model in ['lstm', 'mlp']:
        data_type = 'features'
        image_shape = None
    else:
        raise ValueError("Invalid model. See train.py for options.")
    
    used_sequence_length = 1 * seq_length
    
    firstFrame = 0
    lastFrame = used_sequence_length
    path_to_last_frame = Path('video_processed_tmp\\' + video_name.split('\\')[-1].split('.')[0] + '-' + '%04d' % lastFrame + '.jpg')
    firstGoalFrame = 0
    lastGoalFrame = 0
    sequences_with_goal = [[]]
    goal_sequence = []
    result = False
    
    # do some clipping to also process short videos
    if not path_to_last_frame.is_file():
        used_sequence_length = seq_length
        lastFrame = used_sequence_length
        path_to_last_frame = Path('video_processed_tmp\\' + video_name.split('\\')[-1].split('.')[0] + '-' + '%04d' % lastFrame + '.jpg')
        
    # while we're not yet at the end of the video video, use a jumping window to predict for each interval if its a goal or not
    while(lastFrame <= lastVideoFrame - 1):
        print('current interval: frames ' + str(firstFrame) + ' to ' + str(lastFrame - 1))
        
        result = predict(data_type, seq_length, saved_model, image_shape, video_name, class_limit, firstFrame, lastFrame)
        if(result == True):
            sequences_with_goal[0].append(firstFrame)
        
        firstFrame = firstFrame + used_sequence_length
        lastFrame = lastFrame + used_sequence_length
        path_to_last_frame = Path('video_processed_tmp\\' + video_name.split('\\')[-1].split('.')[0] + '-' + '%04d' % lastFrame + '.jpg')
        
        #otherwise, VRAM runs full -> OOM
        backend.clear_session()
        
        print('###################')
        print('progress: ' + '%.2f' % (100 * firstFrame/lastVideoFrame) + '%')
        print('###################')
    
    # if the whole video is a goal, save the whole video
    if firstGoalFrame > lastGoalFrame and len(sequences_with_goal) >= 1:
        sequences_with_goal.append([firstGoalFrame, lastFrame - used_sequence_length])
    
    print(sequences_with_goal)
    
    current_list_index = 0
    if len(sequences_with_goal) > 0:
        goal_sequence = sequences_with_goal[0]
        sequences_with_goal = []
    tmp_frame_list = []
    split_points = [0, len(goal_sequence)]
    
    for frame in range(len(goal_sequence) - 1):
        if (goal_sequence[frame+1] - goal_sequence[frame]) > (2*goal_offset + 1):
            print(frame + 1)
            split_points.insert(len(split_points)-1,frame + 1)
                
    #print(split_points)
    
    for point in range(len(split_points) - 1):
        #print(point)
        if goal_sequence:
            buffer_until_end = lastVideoFrame - goal_sequence[split_points[point]]
            buffer_from_beginning = goal_sequence[split_points[point]] - 0
        
            if (goal_sequence[split_points[point]] + goal_offset) <= lastVideoFrame and (goal_sequence[split_points[point]] -goal_offset) >= 0:
                if (split_points[point+1] > split_points[point]):
                    sequences_with_goal.append([goal_sequence[split_points[point]] -goal_offset, goal_sequence[split_points[point+1] -1] + goal_offset])
                else:
                    sequences_with_goal.append([goal_sequence[split_points[point]] -goal_offset, goal_sequence[split_points[point]] + goal_offset])  
            elif (goal_sequence[split_points[point]] + goal_offset) <= lastVideoFrame:
                if (split_points[point+1] > split_points[point]):
                    sequences_with_goal.append([goal_sequence[split_points[point]] - buffer_from_beginning, goal_sequence[split_points[point+1] -1] + goal_offset])
                else:
                    sequences_with_goal.append([goal_sequence[split_points[point]] - buffer_from_beginning, goal_sequence[split_points[point]] + buffer_until_end])
            elif (goal_sequence[split_points[point]] -goal_offset) >= 0:
                if (split_points[point+1] > split_points[point]):
                    sequences_with_goal.append([goal_sequence[split_points[point]] -goal_offset, goal_sequence[split_points[point+1] -1]+ buffer_until_end])
                else:
                    sequences_with_goal.append([goal_sequence[split_points[point]] -goal_offset, goal_sequence[split_points[point]]+ buffer_until_end])
            else:
                if (split_points[point+1] > split_points[point]):
                    sequences_with_goal.append([goal_sequence[split_points[point]] - buffer_from_beginning, goal_sequence[split_points[point+1] -1] + buffer_until_end])
                else:
                    sequences_with_goal.append([goal_sequence[split_points[point]] - buffer_from_beginning, goal_sequence[split_points[point]] + buffer_until_end])
        
    #print(sequences_with_goal)
    
    # save the parts with goals in them as numbered videos
    current_goal = 1
    for goal in sequences_with_goal:
        extract_external_files.generate_video_from_framelist(video_name, str(current_goal), goal, framerate, sequence_length_modificator)
        current_goal = current_goal + 1
    
    extract_external_files.delete_generated_image_files("video_processed_tmp")
    
    return (result, video_name)

    
if __name__ == '__main__':
    main(*sys.argv)
