from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
from models import ResearchModels
from data import DataSet
from keras.models import load_model
import time
import os.path
import tensorflow as tf
import keras.backend.tensorflow_backend
import extract_files
import random
import numpy as np
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.datasets import make_classification
from keras.layers import *
from keras.layers.recurrent import LSTM
from keras.models import Sequential, load_model
from keras.optimizers import Adam, RMSprop, SGD, Adamax, Nadam
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import (Conv2D, MaxPooling3D, Conv3D,
    MaxPooling2D)
from collections import deque
from keras.applications import vgg16, inception_v3, resnet50, mobilenet
import sys


data = DataSet(
            seq_length=40,
            class_limit=2,
            image_shape=(240,135, 3)
        )

# get sequences and labels to train on
features, target = data.get_all_sequences_in_memory('train', 'images')
                                       
                                       
#define the model we want to use                                       
def create_network():
    
    input_shape = (40, 240, 135, 3)
    
    model = Sequential()
    
    model.add(ConvLSTM2D(filters=40, kernel_size=(2, 2), strides=(2, 2),
        activation='tanh', padding='same', return_sequences=False,
                   input_shape=input_shape,
                   dropout=0.5))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    
    
    model.add(Flatten())
    
    model.add(Dense(512, activation='tanh'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation='softmax'))
    
    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=2.0*1e-5, decay=0.0*1e-6, amsgrad=False),
                           metrics=['accuracy'])
                           
    return model
    
    
                                       
                                       
# Wrap Keras model so it can be used by scikit-learn
neural_network = KerasClassifier(build_fn=create_network, 
                                 epochs=30, 
                                 batch_size=1, 
                                 verbose=0)
                                 
                                 
                                 
# Evaluate neural network using k-fold cross-validation (with cv = k)
print(cross_val_score(neural_network, features, target, cv=4)                                 )