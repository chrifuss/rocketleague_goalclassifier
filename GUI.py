import tkinter as tk
from tkinter import filedialog
from tkinter import ttk  # Normal Tkinter.* widgets are not themed!
from ttkthemes import ThemedTk
import subprocess
import demo

class Example(ttk.Frame):
    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)

        
        entryText = ""
        self.offsetText = tk.StringVar(self, value='120')
        self.modelText = tk.StringVar(self, value = 'data/checkpoints/convlstm-images.074-0.000.h5')

        # create a prompt, an input box, an output label,
        # and a button to do the computation
        self.prompt = ttk.Label(self, text="\t Enter the path to the video file (including the file name): \t\t", anchor="w")
        self.entry = ttk.Entry(self, textvariable=entryText)
        self.chooseFile = ttk.Button(self, text="Choose video file", width = 20, command = self.openFileButtonPressed)
        
        self.offset_prompt = ttk.Label(self, text="\t Enter the offset before and after each goal (in frames): \t\t", anchor="w")
        self.offset_entry = ttk.Entry(self, textvariable=self.offsetText, width = 10)
        
        self.model_prompt = ttk.Label(self, text="\t Enter the relative path to the model checkpoint to be used (so the path starts with data/checkpoints/): \t\t", anchor="w")
        self.model_entry = ttk.Entry(self, textvariable=self.modelText, width = 10)
        self.chooseModel = ttk.Button(self, text="Choose model file", width = 20, command = self.chooseModelButtonPressed)
        
        self.submit = ttk.Button(self, text="Start", command = self.startButtonPressed)
        self.output = ttk.Label(self, text="")
        self.output2 = ttk.Label(self, text="")

        # lay the widgets out on the screen. 
        self.prompt.pack(side="top", fill="x", pady=10)
        self.entry.pack(side="top", fill="x", padx=10, pady=10)
        self.chooseFile.pack(side="top", padx=10, pady=10)
        self.offset_prompt.pack(side="top", fill="x", pady=10)
        self.offset_entry.pack(side="top", padx=10, pady=10)
        self.model_prompt.pack(side="top", fill="x", pady=10)
        self.model_entry.pack(side="top", fill="x", padx=10, pady=10)
        self.chooseModel.pack(side="top", padx=10, pady=10)
        self.output.pack(side="top", fill="x", expand=True)
        self.output2.pack(side="top", fill="x", expand=True)
        self.submit.pack(side="right")

        
    def openFileButtonPressed(self):
        
        result = filedialog.askopenfilename()
        result = result.replace('/', '\\')
        print('Filepath from GUI: ' + result)
        #self.entry = ttk.Entry(self, textvariable=result)
        self.entry.delete(0, len(self.entry.get()))
        self.entry.insert(0, result)

        
    def chooseModelButtonPressed(self):
        
        result = filedialog.askopenfilename()
        self.model_entry.delete(0, len(self.model_entry.get()))
        self.model_entry.insert(0, result)
        
    def startButtonPressed(self):
        
        #result = subprocess.check_output("python demo.py " + self.entry.get())
        #result = subprocess.call("python demo.py " + self.entry.get())
        result = demo.main(self.entry.get(), goal_offset=self.offset_entry.get(), saved_model= self.model_entry.get())

        # set the output widget to have our result
        self.output.configure(text="Done!")
        self.output2.configure(text="Used file:  " + result[1])

# if this is run as a program (versus being imported),
# create a root window and an instance of our example,
# then start the event loop

if __name__ == "__main__":
    root = ThemedTk(theme="breeze")
    #root = tk.Tk()
    root.title('Goal predictor')
    Example(root).pack(fill="both", expand=True)
    root.mainloop()